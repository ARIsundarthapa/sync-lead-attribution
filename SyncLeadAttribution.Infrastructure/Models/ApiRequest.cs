﻿namespace SyncLeadAttribution.Infrastructure.Models {
    public class ApiRequest {
        public string ApiUrl { get; set; }
        public bool RequiresToken { get; set; }
        public string TokenUrl { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Scope { get; set; }
    }
}

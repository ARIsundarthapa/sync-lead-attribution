﻿using Newtonsoft.Json;

namespace SyncLeadAttribution.Infrastructure.Models {
    public struct AccessToken {
        [JsonProperty("access_token")]
        public string Token { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("token_type")]
        public string Type { get; set; }
    }
}

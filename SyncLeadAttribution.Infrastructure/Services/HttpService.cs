﻿using Newtonsoft.Json;
using SyncLeadAttribution.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SyncLeadAttribution.Infrastructure.Services {
    public class HttpService : IHttpService {
        private readonly HttpClient _httpClient;
        public HttpService(HttpClient httpClient) {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<HttpContent> GetAsync(ApiRequest request) {
            var requestMsg = await GetHttpRequestMessage(request);
            var response = await _httpClient.SendAsync(requestMsg);
            if (response.IsSuccessStatusCode) {
                return response.Content;
            }

            throw new Exception(
                $"Failed to fetch data from url: {request.ApiUrl}. Status code: {response.StatusCode}"
            );
        }

        private async Task<HttpRequestMessage> GetHttpRequestMessage(ApiRequest request) {
            var requestMsg = new HttpRequestMessage(HttpMethod.Get, request.ApiUrl);
            if (request.RequiresToken) {
                var token = await GetTokenAsync(request);
                requestMsg.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            return requestMsg;
        }

        public async Task<string> GetTokenAsync(ApiRequest request) {
            var uriRequest = new Uri(request.TokenUrl);

            var body = new FormUrlEncodedContent(
                new[]
                {
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                    new KeyValuePair<string, string>("client_id", request.ClientId),
                    new KeyValuePair<string, string>("client_secret", request.ClientSecret ),
                    new KeyValuePair<string, string>("scope", request.Scope)
                }
            );

            var response = await _httpClient.PostAsync(uriRequest, body);
            if (response.IsSuccessStatusCode) {
                var content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<AccessToken>(content);
                return result.Token;
            }

            throw new Exception(
                $"Failed to fetch access token from url: {request.TokenUrl}, clientId: {request.ClientId} and scope: {request.Scope}. Status code: {response.StatusCode}"
            );
        }
    }
}

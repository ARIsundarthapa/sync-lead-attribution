﻿using SyncLeadAttribution.Infrastructure.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace SyncLeadAttribution.Infrastructure.Services {
    public interface IHttpService {
        Task<HttpContent> GetAsync(ApiRequest request);
    }
}

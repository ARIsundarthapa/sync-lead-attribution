﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using SyncLeadAttribution.Infrastructure.Models;
using SyncLeadAttribution.Infrastructure.Services;
using System.IO;
using System.Net.Http;

namespace SyncLeadAttribution {
	class Program {
		public static void Main(string[] args) {
			IConfigurationRoot configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: false)
#if DEBUG
				.AddJsonFile("appsettings.Development.json", optional: false)
#endif
				.Build();

			var connectionString = configuration.GetConnectionString("B2CLite");

			var SyncConfig = new ApiRequest();
			configuration.GetSection("SyncConfig").Bind(SyncConfig);

			var logger = new LoggerConfiguration()
				.ReadFrom.Configuration(configuration)
				.CreateLogger();

			var serviceProvider = new ServiceCollection()
				.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(logger, dispose: true))
				.AddSingleton<IHttpService>(new HttpService(new HttpClient()))
				//.AddTransient(sp => new Process(new LeadService( new HttpService(new HttpClient()),new Logger<LeadService>()), GlobalAnalyticsService(), sp.GetRequiredService<ILogger<Process>>()))
				.BuildServiceProvider();

			serviceProvider.GetService<Process>().Run().GetAwaiter().GetResult();
		}
	}
}

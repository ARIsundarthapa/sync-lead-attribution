﻿namespace SyncLeadAttribution.Configuration {
	public class SharepointConfiguration {
		public string Url { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string GroupPath { get; set; }
		public string DocumentLibraryTitle { get; set; }
		public string SharepointSyncConfigDirectory { get; set; }
		public int HoursToCheckModifiedStatus { get; set; }
	}
}

﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SyncLeadAttribution.Infrastructure.Helper;
using SyncLeadAttribution.Infrastructure.Models;
using SyncLeadAttribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncLeadAttribution.Infrastructure.Services {
    public class GlobalAnalyticsService : IGlobalAnalyticsService {
        private const string LeadVisitDetailEndPoint = "/api/Account/{param}/Assets";

        private static ILogger<LeadService> _logger;
        private readonly IHttpService _httpService;
        private readonly ApiRequest _apiRequest;

        public GlobalAnalyticsService(IOptions<ApiRequest> apiRequest, IHttpService httpService, ILogger<LeadService> logger) {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _httpService = httpService ?? throw new ArgumentNullException(nameof(httpService));
            _apiRequest = apiRequest.Value ?? throw new ArgumentNullException(nameof(apiRequest.Value));
        }

       public async Task<IReadOnlyCollection<LeadVisitDetail>> GetLeadVisitDetails(string[] siteIds, DateTime startDate, DateTime endDate) {
            try {
                var apiUrl = ApiHelper.GetUrl(_apiRequest.ApiUrl, LeadVisitDetailEndPoint);
                var accountApiRequest = ApiHelper.GetApiRequest(_apiRequest, apiUrl);
                var content = await _httpService.GetAsync(accountApiRequest);
                if (content != null) {
                    return JsonConvert.DeserializeObject<IReadOnlyCollection<LeadVisitDetail>>(await content.ReadAsStringAsync());
                }
            } catch (Exception ex) {
                _logger.LogError(ex, $"Error in GetLeadVisitDetails");
            }

            return Enumerable.Empty<LeadVisitDetail>().ToList();
        }
    }
}
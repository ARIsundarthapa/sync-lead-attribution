﻿using SyncLeadAttribution.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SyncLeadAttribution.Infrastructure.Services {
    public interface IGlobalAnalyticsService {
        Task<IReadOnlyCollection<LeadVisitDetail>> GetLeadVisitDetails(string[] siteIds, DateTime startDate, DateTime endDate);
    }
}

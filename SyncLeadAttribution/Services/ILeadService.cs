﻿using SyncLeadAttribution.Models;
using System.Threading.Tasks;

namespace SyncLeadAttribution.Infrastructure.Services {
    public interface ILeadService {
        Task<bool> UpdateLeadVisitDetail(LeadVisitDetail leadVisitDetail);
    }
}

﻿using SyncLeadAttribution.Infrastructure.Models;
using System;
namespace SyncLeadAttribution.Infrastructure.Helper {
    public static class ApiHelper {
        public static string GetUrl(string baseUrl, string path = "", string param = "") {
            var url = $"{baseUrl}{path?.Replace("{param}", param)}";
            Uri.TryCreate(url, UriKind.Absolute, out var uri);
            return uri?.AbsoluteUri.TrimEnd('/') ?? string.Empty;
        }

        public static ApiRequest GetApiRequest(ApiRequest request, string url, bool requiresToken = true) {
            return new ApiRequest {
                ApiUrl = url,
                RequiresToken = requiresToken && request.RequiresToken,
                TokenUrl = request.TokenUrl,
                ClientId = request.ClientId,
                ClientSecret = request.ClientSecret,
                Scope = request.Scope
            };
        }
    }
}

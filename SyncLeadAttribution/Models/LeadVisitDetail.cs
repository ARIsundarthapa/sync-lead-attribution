﻿using System;

namespace SyncLeadAttribution.Models {
	public class LeadVisitDetail {
		public int LeadId { get; set; }
		public DateTime VisitDate { get; set; }
		public string ReferrerType { get; set; }
		public string ReferrerNameOrUrl { get; set; }
	}
}

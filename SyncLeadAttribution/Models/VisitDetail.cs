﻿using System;

namespace SyncLeadAttribution.Models {
	public class VisitDetail {
		public DateTime VisitDate { get; }
		public string ReferrerType { get; }
		public string ReferrerNameOrUrl { get; }

		public VisitDetail(
			DateTime visitDate,
			string referrerType,
			string referrerNameOrUrl
		) {
			VisitDate = visitDate;
			ReferrerType = referrerType ?? String.Empty;
			ReferrerNameOrUrl = referrerNameOrUrl ?? String.Empty;
		}
	}
}

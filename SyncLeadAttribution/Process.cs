﻿using Microsoft.Extensions.Logging;
using SyncLeadAttribution.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace SyncLeadAttribution {
	public class Process {
		private readonly ILeadService _leadService;
		private readonly IGlobalAnalyticsService _globalAnalyticsService;
		private readonly ILogger<Process> _logger;

		public Process(ILeadService leadService, IGlobalAnalyticsService globalAnalyticsService, ILogger<Process> logger) {
			_leadService = leadService ?? throw new ArgumentNullException(nameof(leadService));
			_globalAnalyticsService = globalAnalyticsService ?? throw new ArgumentNullException(nameof(globalAnalyticsService));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		public async Task Run() {
			try {
				var startDate = DateTime.Now;
				var endDate = DateTime.Now;
				var siteIds = new string[10];
				_logger.LogInformation("--1. Fetch lead visit detail from Global Analytics service--");
				var leadVisitDetails = await _globalAnalyticsService.GetLeadVisitDetails(siteIds, startDate, endDate);
				_logger.LogInformation($"Lead visit downloaded: {leadVisitDetails.Count}");

				_logger.LogInformation("--2. Send lead visit details to Leads service--");
				int i = 0;
				foreach (var leadVisitDetail in leadVisitDetails) {
					i++;
					try {
						_logger.LogDebug($" Sending {i}/{leadVisitDetails.Count}, lead id: {leadVisitDetail.LeadId}");
						var result = _leadService.UpdateLeadVisitDetail(leadVisitDetail);
						_logger.LogDebug($" Sent lead Id: {leadVisitDetail.LeadId}");						
					} catch (Exception ex) {
						_logger.LogError(ex, $"Error in sending {i}/{leadVisitDetails.Count}, lead Id: {leadVisitDetail.LeadId}.");
					}
				}
				_logger.LogInformation("--Lead visit details processed successfully--");
			} catch (Exception ex) {
				_logger.LogError(ex, "Error occurred in the sync process.");
			}
		}
	}
}

using Microsoft.Extensions.Logging;
using Moq;
using System.Diagnostics;
using Xunit;

namespace SyncLeadAttribution.Tests {
	public class ProcessTests {
		private readonly Mock<ILogger<Process>> _logger;

		public ProcessTests() {
			_logger = new Mock<ILogger<Process>>();
		}

		[Fact]
		public void Run_FilesExistsInSharepointWithInvalidJson_NoUpdatesInDatabase() {
		}

	}
}

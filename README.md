# README #

Add Title

### Where does this app live and write to? ###

--Add description

### How do I get set up? ###

* Setup should be as simple as clone, build, run
* Configuration is done via Octopus variables subsitution on appsettings.json
* Dependencies: Newtonsoft, Serilog; all via NuGet
* Deployment instructions
	* clone/build
	* Set up a cron job  in Kubernetes to run on a nightly basis
	
### Who do I talk to? ###

* Sundar.thapa@leadventure.com
* AppDev